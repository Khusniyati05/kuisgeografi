package com.peradaban.kuisgeografi

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider

private const val TAG = "MainActivity"

class MainActivity : AppCompatActivity() {
    private lateinit var falseButton: Button
    private lateinit var trueButton: Button
    private lateinit var backButton: Button
    private lateinit var nextButton: Button
    private lateinit var viewPertanyaan: TextView
    private lateinit var viewNomor: TextView

    private val quizViewModel: QuizViewModel by lazy {
        ViewModelProvider(this,ViewModelProvider.AndroidViewModelFactory(application)).get(QuizViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        falseButton = findViewById(R.id.false_button)
        trueButton = findViewById(R.id.true_button)
        backButton = findViewById(R.id.back_button)
        nextButton = findViewById(R.id.next_button)
        viewNomor = findViewById(R.id.view_nomor)
        viewPertanyaan = findViewById(R.id.view_pertanyaan)

        updateSoal()

        falseButton.setOnClickListener { view: View ->
            cekJawaban(false)
        }
        trueButton.setOnClickListener { view: View ->
            cekJawaban(true)
        }
        backButton.setOnClickListener { view: View ->
            quizViewModel.soalSebelumnya()
            updateSoal()
        }
        nextButton.setOnClickListener { view: View ->
            quizViewModel.soalBerikutnya()
            updateSoal()
        }
    }

    override fun onStart() {
        super.onStart()
        Log.d(TAG, "Fungsi Onstart dipanggil")
    }

    override fun onResume() {
        super.onResume()
        Log.d(TAG, "Fungsi Onresume dipanggil")
    }

    override fun onPause() {
        super.onPause()
        Log.d(TAG, "Fungsi OnPause dipanggil")
    }

    override fun onStop() {
        super.onStop()
        Log.d(TAG, "Fungsi OnStop dipanggil")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d(TAG, "Fungsi OnDestroy dipanggil")
    }

    private fun updateSoal() {
        viewPertanyaan.setText(quizViewModel.soalSekarang)
        viewNomor.setText("${quizViewModel.indexPertanyaan + 1}")
        if (quizViewModel.indexPertanyaan == 0) {
            backButton.visibility = View.INVISIBLE
        } else {
            backButton.visibility = View.VISIBLE
        }
        if (quizViewModel.indexPertanyaan == (quizViewModel.jumlahSoal - 1)) {
            nextButton.visibility = View.INVISIBLE
        } else {
            nextButton.visibility = View.VISIBLE
        }
    }

    private fun cekJawaban(jawab:Boolean) {
        var jawaban = quizViewModel.jawabanSekarang
        if (jawaban == jawab) {
            Toast.makeText(this,"Jawaban anda benar",Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(this,"Jawaban anda salah",Toast.LENGTH_SHORT).show()
        }
    }
}