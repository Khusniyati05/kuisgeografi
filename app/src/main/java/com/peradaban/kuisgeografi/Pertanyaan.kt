package com.peradaban.kuisgeografi

import androidx.annotation.StringRes

data class Pertanyaan(@StringRes val pertanyaan: Int, val jawaban: Boolean)