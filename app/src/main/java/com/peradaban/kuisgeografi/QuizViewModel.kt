package com.peradaban.kuisgeografi

import android.util.Log
import androidx.lifecycle.ViewModel

private const val TAG = "QuizViewModel"
class QuizViewModel : ViewModel() {
    var indexPertanyaan = 0
    private val listPertanyaan = listOf(
        Pertanyaan(R.string.pertanyaan1,true),
        Pertanyaan(R.string.pertanyaan2,true),
        Pertanyaan(R.string.pertanyaan3,true),
        Pertanyaan(R.string.pertanyaan4,true),
        Pertanyaan(R.string.pertanyaan5,true),
    )
    val jawabanSekarang: Boolean
        get() = listPertanyaan[indexPertanyaan].jawaban
    val soalSekarang: Int
        get() = listPertanyaan[indexPertanyaan].pertanyaan
    val jumlahSoal: Int
        get() = listPertanyaan.size
    fun soalSebelumnya() {
        indexPertanyaan = indexPertanyaan - 1
    }
    fun soalBerikutnya() {
        indexPertanyaan = indexPertanyaan + 1
    }
}